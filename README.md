# PHP Facebook

Extends league/oauth2-facebook with methods for posting to pages
https://packagist.org/packages/league/oauth2-facebook

<!-- writeme -->
Php-facebook
============

Extends the Facebook OAuth 2.0 Client Provider with methods for posting to Facebook

 * https://gitlab.com/agaric/php/php-facebook
 * Keywords: oauth, oauth2, client, authorization, authentication, facebook, post
 * Package name: agaric/php-facebook


### Maintainers

 * keegan rankin - https://agaric.coop/keegan
 * benjamin melançon - https://agaric.coop/mlncn


### Requirements

 * league/oauth2-facebook ^2.0


### License

AGPL-3.0-or-later

<!-- endwriteme -->

## Installation

Recommended to install with composer.

## Usage

See for an example implementation see https://git.drupalcode.org/project/social_post_facebook

## Support

PHP Facebook is supported by Agaric, a small web development collective, and not Facebook, a giant evil corporation so faceless it has now gone nameless.

You can reach Agaric for paid support or development https://agaric.coop/ask

## Roadmap

  - Possibly extending to help collect information on shares.

Other than that, no further development planned— end of the road.  We hope to put our efforts into the mass transit of IndieWeb and 

## Contributing

https://gitlab.com/agaric/php/php-facebook/-/issues
